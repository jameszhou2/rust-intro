mod days;
mod luhn;

fn main() {
    days::day1::run();
    days::day2::run();
    days::day3::run();
    days::day4::run();
    days::day5::run();
    days::day10::run();
}
