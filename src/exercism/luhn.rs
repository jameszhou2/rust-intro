fn is_valid(code: &str) -> bool {
    if code.trim().len() <= 1 {
        return false;
    }

    code.chars()
        .rev()
        .filter(|c| !c.is_whitespace())
        .map(|c| c.to_digit(10).unwrap())
        .enumerate()
        .map(|(i, x)| if i % 2 == 1 { x * 2 } else { x })
        .map(|x| if x > 9 { x - 9 } else { x })
        .sum::<u32>() % 10
        == 0
}

pub fn main() {
    is_valid();
}
