pub struct Luhn {
  code: String,
}

impl Luhn {
    pub fn is_valid(&self) -> bool {
        if self.code.trim().len() <= 1 {
            return false;
        }

    self.code
        .chars()
        .rev()
        .filter(|c| !c.is_whitespace())
        .map(|c| c.to_digit(10).unwrap())
        .enumerate()
        .map(|(i, x)| if i % 2 == 1 { x * 2 } else { x })
        .map(|x| if x > 9 { x - 9 } else { x })
        .sum::<u32>() % 10
        == 0
    }
}

impl From<String> for Luhn {
    fn from(s: String) -> Self {
        Luhn { code: s }
    }
}

impl From<&str> for Luhn {
    fn from(s: &str) -> Self {
        Luhn { code: s.to_string() }
    }
}

impl From<u32> for Luhn {
    fn from(n: u32) -> Self {
        Luhn { code: n.to_string() }
    }
}

pub fn main() {
    let number: u128 = 4539319503436467;
    println!("Is valid? {}", number.valid_luhn());

    let number = "4539 3195 0343 6467";
    println!("Is valid? {}", number.valid_luhn());

    let number_str = String::from("4539 3195 0343 6467");
    println!("Is valid? {}", number_str.valid_luhn());
}

