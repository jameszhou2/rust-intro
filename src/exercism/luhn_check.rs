pub trait LuhnCheck {
    fn valid_luhn(&self) -> bool;
}

impl LuhnCheck for String {
fn valid_luhn(&self) -> bool {
    let code = self.trim();
    if code.len() <= 1 {
        return false;
    }

    code.chars()
        .rev()
        .filter(|c| !c.is_whitespace())
        .map(|c| c.to_digit(10).unwrap())
        .enumerate()
        .map(|(i, x)| if i % 2 == 1 { x * 2 } else { x })
        .map(|x| if x > 9 { x - 9 } else { x })
        .sum::<u32>() % 10
        == 0
  }
}

impl LuhnCheck for &str {
    fn valid_luhn(&self) -> bool {
        (*self).to_string().valid_luhn()
    }
}

impl LuhnCheck for u32 {
    fn valid_luhn(&self) -> bool {
        self.to_string().valid_luhn()
    }
}

impl LuhnCheck for u64 {
    fn valid_luhn(&self) -> bool {
        self.to_string().valid_luhn()
    }
}

impl LuhnCheck for u128 {
    fn valid_luhn(&self) -> bool {
        self.to_string().valid_luhn()
    }
}

pub fn main() {
    let number = Luhn::from(4539319503436467u32);
    println!("Is valid? {}", number.is_valid());

    let number = Luhn::from("4539 3195 0343 6467");
    println!("Is valid? {}", number.is_valid());

    let number_str = String::from("4539 3195 0343 6467");
    let number = Luhn::from(number_str);
    println!("Is valid? {}", number.is_valid());
}

