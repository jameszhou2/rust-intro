use std::collections::HashMap;

fn main() {
    let mut stacks: HashMap<i32, Vec<char>> = HashMap::new();
    stacks.insert(1, vec!['Z', 'N']);
    stacks.insert(2, vec!['M', 'C', 'D']);
    stacks.insert(3, vec!['P']);

    let procedures = vec![
        (1, 2, 1),
        (3, 1, 3),
        (2, 2, 1),
        (1, 1, 2),
    ];

    for (quantity, from, to) in procedures {
        for _ in 0..quantity {
            let value = stacks.get_mut(&from).unwrap().pop().unwrap();
            stacks.get_mut(&to).unwrap().push(value);
        }
    }

    for i in 1..=3 {
        let stack = stacks.get(&i).unwrap();
        if let Some(&top_crate) = stack.last() {
            print!("{}", top_crate);
        }
    }
}

pub fn run() {
    main();
}