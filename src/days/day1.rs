use std::cmp;

fn find_most_calories(input: &str) -> i32 {
    let elf_calories: Vec<Vec<i32>> = input.split("\n\n")
        .map(|s| s.split_whitespace()
            .filter_map(|word| word.parse().ok())
            .collect())
        .collect();

    let mut max_calories = 0;
    for calories in elf_calories {
        let sum: i32 = calories.iter().sum();
        max_calories = cmp::max(max_calories, sum);
    }
    max_calories
}

fn main() {
    let input = "1000
2000
3000

4000

5000
6000

7000
8000
9000

10000";
    let max_calories = find_most_calories(input);
    println!("The elf carrying the most calories has a total of {} calories.", max_calories);
}

pub fn run() {
    main();
}
