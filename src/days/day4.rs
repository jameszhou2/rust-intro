fn main() {
    let assignments = vec![
        ((2, 4), (6, 8)),
        ((2, 3), (4, 5)),
        ((5, 7), (7, 9)),
        ((2, 8), (3, 7)),
        ((6, 6), (4, 6)),
        ((2, 6), (4, 8)),
    ];

    let mut count = 0;

    for (range1, range2) in assignments {
        if is_contained(range1, range2) || is_contained(range2, range1) {
            count += 1;
        }
    }

    println!("{} pairs where one range fully contains the other", count);
    }

fn is_contained((start1, end1): (i32, i32), (start2, end2): (i32, i32)) -> bool {
    start1 >= start2 && end1 <= end2
}

pub fn run() {
    main();
}