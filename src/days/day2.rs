use std::str::FromStr;

#[derive(Debug)]
enum Shape {
    Rock,      // A or X
    Paper,     // B or Y
    Scissors,  // C or Z
}

impl FromStr for Shape {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "A" | "X" => Ok(Shape::Rock),
            "B" | "Y" => Ok(Shape::Paper),
            "C" | "Z" => Ok(Shape::Scissors),
            _ => Err(()),
        }
    }
}

fn round_score(opponent: Shape, you: Shape) -> i32 {
    let base_score = match you {
        Shape::Rock => 1,
        Shape::Paper => 2,
        Shape::Scissors => 3,
    };

    let outcome_score = match (opponent, you) {
        (Shape::Rock, Shape::Paper) => 6,         
        (Shape::Paper, Shape::Scissors) => 6,     
        (Shape::Scissors, Shape::Rock) => 6,      
        (Shape::Rock, Shape::Scissors) => 0,      
        (Shape::Paper, Shape::Rock) => 0,         
        (Shape::Scissors, Shape::Paper) => 0,     
        _ => 3,                                   
    };

    base_score + outcome_score
}

fn total_score(strategy_guide: &str) -> i32 {
    let strategies: Vec<&str> = strategy_guide.split_whitespace().collect();
    strategies.chunks(2)
        .map(|pair| round_score(Shape::from_str(pair[0]).unwrap(), Shape::from_str(pair[1]).unwrap()))
        .sum()
}

fn main() {
    let strategy_guide = "A Y B X C Z";
    let score = total_score(strategy_guide);
    println!("Your total score would be: {}", score);
}

pub fn run() {
    main();
}