fn main() {
    let rucksacks = vec![
        "vJrwpWtwJgWrhcsFMMfFFhFp",
        "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
        "PmmdzqPrVvPwwTWBwg",
        "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
        "ttgJtRGJQctTZtZT",
        "CrZsJsPPZsGzwwsLwLmpwMDw",
    ];
  
    let mut total_priority = 0;
    
    for rucksack in rucksacks {
        let half_len = rucksack.len() / 2;
        let (first_half, second_half) = rucksack.split_at(half_len);

        for item in first_half.chars() {
            if second_half.contains(item) {
                total_priority += get_priority(item);
                break;
            }
        }
    }
  
    println!("The sum of the priorities is {}", total_priority);
}

fn get_priority(c: char) -> i32 {
    if c.is_ascii_lowercase() {
        c as i32 - 'a' as i32 + 1
    } else {
        c as i32 - 'A' as i32 + 27
    }
}

pub fn run() {
    main();
}
