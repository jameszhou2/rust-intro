use std::str::FromStr;

enum Instruction {
    Noop,
    Addx(i32),
}

impl FromStr for Instruction {
  type Err = ();

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    let parts: Vec<&str> = s.split_whitespace().collect();
    if parts.is_empty() {
      return Err(());
    }
    match parts[0] {
      "noop" => Ok(Instruction::Noop),
      "addx" => {
        if parts.len() < 2 {
          return Err(());
        }
        Ok(Instruction::Addx(parts[1].parse().unwrap()))
      }
      _ => Err(()),
    }
  }
}

fn main() {
  let input = "\
  addx 15
  addx -11
  addx 6
  addx -3
  addx 5
  addx -1
  addx -8
  addx 13
  addx 4
  noop
  addx -1
  addx 5
  addx -1
  addx 5
  addx -1
  addx 5
  addx -1
  addx 5
  addx -1
  addx -35
  addx 1
  addx 24
  addx -19
  addx 1
  addx 16
  addx -11
  noop
  noop
  addx 21
  addx -15
  noop
  noop
  addx -3
  addx 9
  addx 1
  addx -3
  addx 8
  addx 1
  addx 5
  noop
  noop
  noop
  noop
  noop
  addx -36
  noop
  addx 1
  addx 7
  noop
  noop
  noop
  addx 2
  addx 6
  noop
  noop
  noop
  noop
  noop
  addx 1
  noop
  noop
  addx 7
  addx 1
  noop
  addx -13
  addx 13
  addx 7
  noop
  addx 1
  addx -33
  noop
  noop
  noop
  addx 2
  noop
  noop
  noop
  addx 8
  noop
  addx -1
  addx 2
  addx 1
  noop
  addx 17
  addx -9
  addx 1
  addx 1
  addx -3
  addx 11
  noop
  noop
  addx 1
  noop
  addx 1
  noop
  noop
  addx -13
  addx -19
  addx 1
  addx 3
  addx 26
  addx -30
  addx 12
  addx -1
  addx 3
  addx 1
  noop
  noop
  noop
  addx -9
  addx 18
  addx 1
  addx 2
  noop
  noop
  addx 9
  noop
  noop
  noop
  addx -1
  addx 2
  addx -37
  addx 1
  addx 3
  noop
  addx 15
  addx -21
  addx 22
  addx -6
  addx 1
  noop
  addx 2
  addx 1
  noop
  addx -10
  noop
  noop
  addx 20
  addx 1
  addx 2
  addx 2
  addx -6
  addx -11
  noop
  noop
  noop
  ";

  let instructions: Vec<_> = input
  .lines()
  .filter(|line| !line.trim().is_empty() && !line.starts_with("//"))  // Filter out empty and comment lines
  .map(|line| line.parse().unwrap())
  .collect();
  let mut cycle = 0;
  let mut x = 1;
  let mut signal_strengths = vec![];
  let interesting_cycles = vec![20, 60, 100, 140, 180, 220];
  let mut visited_cycles = vec![false; interesting_cycles.len()];

  for inst in instructions {
    match inst {
      Instruction::Noop => cycle += 1,
      Instruction::Addx(v) => {
          cycle += 2;
          x += v;
      }
    }

    if let Some(index) = interesting_cycles.iter().position(|&c| c == cycle) {
      if !visited_cycles[index] {
        signal_strengths.push(cycle * x);
        visited_cycles[index] = true;
      }
    }
  }

let sum: i32 = signal_strengths.iter().sum();
  println!("Sum of the signal strengths: {}", sum);
}

pub fn run() {
main();
}